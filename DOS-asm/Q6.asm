.model small
.stack
.data
	enterNum db 0ah, "Enter the number in ACII: $"
	printNum db 0ah, "The number entered is: $"
	number db 0
.code
conv proc stdcall
	mov dx, offset enterNum
	mov ah, 09h
	int 21h
	mov ah, 01h
	int 21h
	mov bl, al
	mov dx, offset printNum
	mov ah, 09h
	int 21h
	mov cx, 8
	again:
		xor bh, bh
		shl bx, 1
		add bh, 30h
		mov dl, bh
		mov ah, 02h
		int 21h
		loop again	
	ret
conv endp
start:
	mov dx, @data
	mov ds, dx
	mov es, ax
	call conv
	mov ah, 4ch
	int 21h
end start