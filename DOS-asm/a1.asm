.model small
.stack
.data
hello db 'Hello World!', 00ah, '$'

.code

mov ax, @data			; Moving data to ax
mov ds, ax				; Moving ax to ds
mov ah, 09h				; Moving SYS_CALL STDOUT (09h) to AH
mov dx, OFFSET hello 	; Moving data from Offset to dx
int 21h					; Calling Kernel
mov ah, 04ch			; SYS_CALL EXIT (04ch)
int 21h					; Calling Kernel
end