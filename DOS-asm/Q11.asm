.model small
.stack
.data
number1	dd	12345678h
number2	dd	22222222h
result	dq	0000000000000000h
.code
start:
	mov ax, @data
	mov ds, ax
	mov ax, word ptr number1
	mul word ptr number2
	mov word ptr result, ax
	mov cx, dx
	mov ax, word ptr number1+2
	mul word ptr number2
	add cx, ax
	mov bx, dx
	adc bx, 0

	mov ax, word ptr number1
	mul word ptr number2+2
	add cx, ax
	mov word ptr result+2, cx
	adc bx, dx
	mov ax, word ptr number1+2
	mul word ptr number2+2
	add bx, ax
	mov word ptr result+4, bx
	mov cx, dx
	adc cx, 0
	mov word ptr result+6, cx
	mov ah, 4ch
	int 21h
end start

