.model small

.stack

.data
	buffer   db 32
			 db 33 dup(?)
	msg1 db 'Enter a string:', 0ah, '$'
	msg2 db 'You Entered: ', 0ah, '$'
.code
.startup

mov ax, @data
mov ds, ax
mov dx, OFFSET msg1 ; Print before input
mov ah, 09h
int 21h

mov ax, @data
mov ds, ax
mov ah, 0ah
mov dx, OFFSET buffer
int 21h

mov dx, OFFSET msg2
mov ah, 09h
int 21h

mov ax, @data
mov ds, ax
mov bx, OFFSET buffer
mov al, [bx+1]
add al, 02
xor ah, ah
mov si, ax
mov byte [bx+si], '$'
mov dx, OFFSET buffer
add dx, 02
mov ah, 09h
int 21h

mov ah, 04ch
int 21h
end