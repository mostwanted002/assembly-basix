.model small
.stack
.data
	enterNum db 0ah, "Enter the number in binary: $"
	printNum db 0ah, "The number entered is: $"
	number db 0
.code
	conv proc stdcall
		mov cx, 8
		mov dx, offset enterNum
		mov ah, 09h
		int 21h
		again:
			xor dx,dx
			mov ah, 01h
			int 21h
			and al, 0fh
			shl bl, 1
			or bl, al
			loop again
			mov byte ptr number, bl
			mov dx, offset printNum
			mov ah, 09h
			int 21h
			mov dl, byte ptr number
			mov ah, 02h
			int 21h
			ret
	conv endp
	start:
		mov dx, @data
		mov ds, dx
		mov es, ax
		call conv
		mov ah, 4ch
		int 21h
	end start
