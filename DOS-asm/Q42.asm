.model small
.stack
.data
array1 db 12h, 24h, 36h, 48h, 60h
array2 db 60h, 48h, 36h, 24h, 12h
.code

sbar proc stdcall
	mov dl, array1[di]
	sub array2[di], dl
	inc di
	dec al
	jnz adar
	ret
sbar endp
start:
	mov dx, @data
	mov ds, dx
	mov es, ax
	xor dx, dx
	mov ax, 0005h
	mov di, 00h
	call sbar
	mov ah, 4ch
	int 21h
end start
