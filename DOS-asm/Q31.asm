.model small
.stack
.data
	array db 61, 62, 63, 64, 65, 66, 67, 68, 69
	found db 0ah, 'FOUND!$'
	notfound db 0ah, 'NOT FOUND!$'
	enterc db 0ah, 'Enter the char to look for: $'
.code

	lsearch proc stdcall
		mov dx, offset enterc
		mov ah, 09h
		int 21h
		mov ah, 01h
		int 21h
		mov cx, 09h
        lea di, array
        repne scasb
        jne notfoundi
        mov dx, offset found
		mov ah, 09h
		int 21h
		ret
	lsearch endp

	start:
		mov dx, @data
		mov ds, dx
		mov es, ax
		call lsearch
		mov ah, 4ch
		int 21h
	notfoundi:
		mov dx, offset notfound
		mov ah, 09h
		int 21h
		mov ah, 4ch
		int 21h
	end start