.model small
.stack
.data
number1 dd 24681234h
number2 dd 12342468h
result dq 0

.code
start:
	mov dx, @data
	mov ds, dx
	mov es, ax
	mov dx, 0000h
	mov al, byte ptr number1
	sub al, byte ptr number2
	das
	mov byte ptr result, al
	mov al, byte ptr number1+1
	sbb al, byte ptr number2+1
	das
	mov byte ptr result+1, al
	mov al, byte ptr number1+2
	sbb al, byte ptr number2+2
	das
	mov byte ptr result+2, al
	mov al, byte ptr number1+3
	sbb al, byte ptr number2+3
	das
	mov byte ptr result+3, al
	xor al,al
	sbb al, 0
	mov byte ptr result+4, al
	mov ah, 4ch
	int 21h
end start