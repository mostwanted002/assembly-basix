.model small
.stack
.data
number1 dd 11112222h
number2 dw 1111h
result dd 0
remainder dw 0

.code
start:
	mov dx,@data
	mov ds,dx
	mov es,dx
	mov ax, word ptr[number1+2]
	mov dx, 0000h
	div word ptr[number2]
	mov byte ptr[result+2],al
	mov ax,word ptr[number1]
	mov dx, 0000h
	div word ptr[number2]
	mov word ptr[result], ax
	mov word ptr[remainder], dx
	mov ah, 4ch
	int 21h
end start
