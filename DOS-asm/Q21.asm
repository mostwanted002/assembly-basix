.model small
.stack
.data
number1 dd 12342468h
number2 dd 24681234h
result dq 0

.code
start:
	mov dx, @data
	mov ds, dx
	mov es, ax
	mov dx, 0000h
	mov al, byte ptr number1
	add al, byte ptr number2
	daa
	mov byte ptr result, al
	mov al, byte ptr number1+1
	adc al, byte ptr number2+1
	daa
	mov byte ptr result+1, al
	mov al, byte ptr number1+2
	adc al, byte ptr number2+2
	daa
	mov byte ptr result+2, al
	mov al, byte ptr number1+3
	adc al, byte ptr number2+3
	daa
	mov byte ptr result+3, al
	xor al,al
	adc al, 0
	mov byte ptr result+4, al
	mov ah, 4ch
	int 21h
end start