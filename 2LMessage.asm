section .text:
    global _start   ;declared for linker

_start:

    mov edx,len_msg1    ;length of message 1 to edx
    mov ecx,msg1        ;moving message 1 to ecx
    mov ebx,1           ;STDOUT File Descriptor to ebx
    mov eax,4           ;SYS_WRITE System Call to eax
    int 0x80
    
    mov edx,len_msg2    ;length of message 2 to edx
    mov ecx,msg2        ;moving message 2 to ecx
    mov ebx,1           ;STDOUT File Descriptor to ebx
    mov eax,4           ;SYS_WRITE System Call to eax
    int 0x80            ;Calling Kerne
    
    mov eax,1           ;SYS_EXIT to eax
    int 0x80            ;Calling kernel
    
section .data:
    msg1 db 'This is message 1', 0xa    ;message 1 '\n'
    len_msg1 equ $ - msg1               ;length of message 1
    msg2 db 'This is message 2', 0xa    ;message 2 '\n'
    len_msg2 equ $ - msg2               ;length of message 2
    
